<?php
/**
 * Created by PhpStorm.
 * User: WebDev04
 * Date: 2018-02-20
 * Time: 오후 1:30
 */
/** 디버깅용 기능 추가  */
if ( ! function_exists('vdp')) {
    function vdp($obj)
    {
        echo "<pre>";
        var_dump($obj);
        echo "</pre>";
    }
}

if ( ! function_exists('get_time')) {
    function get_time()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}

/** 디버깅용 기능 추가  */

function getFileSize($size, $float = 0) {
    $unit = array('Byte', 'KB', 'MB', 'GB', 'TB');
    for ($L = 1; intval($size / 1024) > 0; $L++, $size/= 1024);
    if (($float === 0) && (intval($size) != $size)) $float = 2;
    return number_format($size, $float, '.', ',') .' '. $unit[$L];
}

