<?php
/**
 * Created by PhpStorm.
 * User: WebDev04
 * Date: 2018-03-14
 * Time: 오전 10:59
 */
class DefaultService
{

    protected $CI;
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }
    /**
     * 로그인 처리
     */
    public function loginProcess($param){
        $loginResult = $this->CI->DefaultModel->LoginCheck($param);

        $result['result'] = false;
        $result['msg'] = 'Fail';

        if($loginResult!=false){
            /** 아이디 저장하기 */
            if(isset($param['IdSave'])){
                $idSaveCookie = array(
                    'name'   => 'userSaveId',
                    'value'  => $param['admin_id'],
                    'expire' => '86400',
                    'path' =>  "/"
                );
                $this->CI->input->set_cookie($idSaveCookie);
            }else{
                delete_cookie('userSaveId');
            }
            $this->CI->session->set_userdata(['userInfo'=>$loginResult]);
            $result['result'] = true;
            $result['msg'] = 'Success';
        }

        return $result;
    }


    public function getAdmin(){
        return $this->CI->DefaultModel->GetAdmin();
    }

    /**
     * $upload_dir : 업로드 경로
     * $field_name : <input> 태그의 name 값
     * $multi_files : 업로드파일 배열 정보
     * $multi_index : 배열 index 값
     */
    function multi_file_upload($field_name,$upload_dir, $_file )
    {
        $config['upload_path'] = realpath($upload_dir);
        $config['allowed_types'] = '*';
        $config['max_size']             =  2048000;
        $config['encrypt_name']         =  true;

        $this->CI->load->library('upload', $config);

        $result = array();
        /** 필드명이 여러개인경우 */
        if(is_array($field_name)){
            foreach($field_name as $field) {
                if (isset($_file[$field]['name'])) {
                    if (is_array($_file[$field]['name'])) {
                        /** 멀티파일 업로드 */
                        $countFileSize = count($_file[$field]['name']);
                        for ($i = 0; $i < $countFileSize; $i++) {
                            $_FILES[$field]['name'] = $_file[$field]['name'][$i];
                            $_FILES[$field]['type'] = $_file[$field]['type'][$i];
                            $_FILES[$field]['tmp_name'] = $_file[$field]['tmp_name'][$i];
                            $_FILES[$field]['error'] = $_file[$field]['error'][$i];
                            $_FILES[$field]['size'] = $_file[$field]['size'][$i];
                            if (!$this->CI->upload->do_upload($field)) {
                                $result[$field][$i]['error'] = explode("|", $this->CI->upload->display_errors('', '|'))[0];
                            } else {
                                $result[$field][$i] = $this->CI->upload->data();
                            }
                        }
                    } else {
                        /** 단일파일 업로드 */
                        $_FILES[$field]['name'] = $_file[$field]['name'];
                        $_FILES[$field]['type'] = $_file[$field]['type'];
                        $_FILES[$field]['tmp_name'] = $_file[$field]['tmp_name'];
                        $_FILES[$field]['error'] = $_file[$field]['error'];
                        $_FILES[$field]['size'] = $_file[$field]['size'];
                        if (!$this->CI->upload->do_upload($field)) {
                            $result[$field]['error'] = $this->CI->upload->display_errors('', '');
                        } else {
                            $result[$field] = $this->CI->upload->data();
                        }
                    }
                }
            }
        }else {
            /** 필드명이 단일 인 경우 */
            if(is_array($_file[$field_name]['name'])){
                /** 멀티 파일 업로드 */
                $countFileSize = count($_file[$field_name]['name']);
                for ($i = 0; $i < $countFileSize; $i++) {
                    $_FILES[$field_name]['name'] = $_file[$field_name]['name'][$i];
                    $_FILES[$field_name]['type'] = $_file[$field_name]['type'][$i];
                    $_FILES[$field_name]['tmp_name'] = $_file[$field_name]['tmp_name'][$i];
                    $_FILES[$field_name]['error'] = $_file[$field_name]['error'][$i];
                    $_FILES[$field_name]['size'] = $_file[$field_name]['size'][$i];
                    if(!$this->CI->upload->do_upload($field_name)){
                        $result[$field_name][$i]['error'] = explode("|",$this->CI->upload->display_errors('','|'))[0];
                    }else{
                        $result[$field_name][$i]=$this->CI->upload->data();
                    }
                }
            }else{
                /** 단일 파일 업로드 */
                $_FILES[$field_name]['name'] = $_file[$field_name]['name'];
                $_FILES[$field_name]['type'] = $_file[$field_name]['type'];
                $_FILES[$field_name]['tmp_name'] = $_file[$field_name]['tmp_name'];
                $_FILES[$field_name]['error'] = $_file[$field_name]['error'];
                $_FILES[$field_name]['size'] = $_file[$field_name]['size'] ;
                if(!$this->CI->upload->do_upload($field_name)){
                    $result[$field_name]['error'] = $this->CI->upload->display_errors('','');
                }else{
                    $result[$field_name]=$this->CI->upload->data();
                }
            }
        }

        return $result;
    }

    public function saveAdmin($param){
        if(isset($param['password'])) {
            if ($param['password'] == "") {
                unset($param['password']);
            }
        }
        $uptResult = $this->CI->DefaultModel->saveAdmin($param);
        $result = array(
            'result' => false,
            'msg' => "fail"
        );
        if($uptResult){
            $result = array(
                'result' => true,
                'msg' => "success"
            );
        }
        return $result;
    }

    /** 예외처리 파일 관련 및 기타  */
    public function monitorSave($param ,$bg_img = "" , $bg_mov = ""){
        $monitorSetupParam = array(
            'templet_type' => $param['templet_type'],
            'text_loc' => $param['text_loc'],
            'text_type' => $param['text_type'],
            'titleFont' => $param['titleFont'],
            'menuFont' => $param['menuFont'],
            'titleSize' => $param['titleSize'],
            'menuSize' => $param['menuSize'],
            'titleColor' => $param['titleColor'],
            'menuColor' => $param['menuColor'],
            'pageEffect' => $param['pageEffect'],
        );
        if($bg_img != '') {
            $monitorSetupParam['bg_img'] = $bg_img;
        }
        if($bg_mov != ''){
            $monitorSetupParam['bg_mov'] = $bg_mov;
        }

        if(isset($param['menuBgUse']) && $param['menuBgUse']=="Y"){
            $monitorSetupParam['menuBgUse'] = "Y";
            $monitorSetupParam['menuBgColor'] = $param['menuBgColor'];
        }else{
            $monitorSetupParam['menuBgUse'] = "N";
        }

        $monitorSetup = $this->CI->DefaultModel->monitorSave($monitorSetupParam,$param['no']);

        $this->CI->DefaultModel->removeMenu($param['no']);

        if(isset($param['menuType'])) {
            foreach ($param['menuType'] as $idx => $val) {
                $tempParam = array(
                    'pageNum' => $param['pageNo'][$idx],
                    'menuType' => $val,
                    'menuName' => $param['menuName'][$idx],
                    'menuPrice' => $param['menuPrice'][$idx],
                    'monitorNum' => $param['no']
                );
                $this->CI->DefaultModel->insertMenu($tempParam);
            }
        }
        return array(
            'result' => true,
            'msg' => '???'
        );
    }

    public function getMonitorSet($monitorNum){
        $monitorInfo = $this->CI->DefaultModel->getMonitorSet($monitorNum);
        $menuInfo = $this->CI->DefaultModel->getMenuText($monitorNum);
        $menuData = array(
            'page1' => false,
            'page2' => false,
            'page3' => false,
        );
        if($menuInfo!=false){
            foreach ($menuInfo as $menu){
                $menuData['page'.$menu['pageNum']][] = $menu;
            }
        }
        return array(
            'set' => $monitorInfo,
            'menu' => $menuData
        );
    }

    public function getFontList(){
        return $this->CI->DefaultModel->getFontList();
    }
}
