<?php
/**
 * Created by PhpStorm.
 * User: WebDev04
 * Date: 2018-03-15
 * Time: 오전 10:46
 */

class UserModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /** 이메일 로 사용자 정보 호출*/
    function getUserInfoByEmail($email_adres){
        $query = "SELECT * FROM 
                    tb_user_info 
                  WHERE email_adres = ? 
                  AND del_yn ='N' AND user_stat_code !='탈퇴' ";
        return $this->getRowArray($query,[$email_adres]);
    }

    function getUserInfoByUserNoReLogin($user_no){
        $query = "SELECT * FROM 
                    tb_user_info 
                  WHERE user_no = ? 
                  AND del_yn ='N' AND user_stat_code !='탈퇴' ";
        return $this->getRowArray($query,[$user_no]);
    }

    /** 로그인 로그 쌓기 */
    function saveUserLoginLog($user_no,$chk){
        $query = "INSERT INTO tb_user_login (
                        user_no
                        ,login_date 
                        ,login_chk 
                        )VALUES(
                          ?
                          ,now()
                          ,?
                        )";
        $param[] = $user_no;
        $param[] = $chk;
        return $this->insertQuery($query,$param);
    }

    /** 로그인 실패 카운트  */
    function loginFailCount($user_no){
        $query = "SELECT 
                        COUNT(*) AS failCount 
                    FROM 
                        tb_user_login AS tul
                    WHERE tul.user_no = ?
                    AND tul.login_date > (
                          SELECT login_date FROM tb_user_login WHERE user_no=tul.user_no AND login_chk='Y' ORDER BY login_date DESC LIMIT 0,1
                          )
                    AND tul.login_chk = 'N'";
        return $this->getOneField($query,"failCount",[$user_no]);
    }

    public function getMaxGroupNo(){
        $query= "SELECT max(group_no)+1 as grpNo FROM tb_user_info_multi_temp";
        return $this->getOneField($query,'grpNo');
    }

    public function insertUserInfoMultiTemp($param){
        $query = "INSERT INTO tb_user_info_multi_temp SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);

        try {
            $result = $this->insertQuery($query,$param);
        } catch(Exception $e) {
            $result = $e->getMessage();
            return $result;
        }

        return $result;
    }

    /** 사용자 정보 삽입 */
    public function insertUserInfo($param){
        $query = "INSERT INTO tb_user_info SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , mod_time = now() ";
        $query .= " , reg_time = now() ";

        try {
            $result = $this->insertQuery($query,$param);
        } catch(Exception $e) {
            $result = $e->getMessage();
            return $result;
        }

        return $result;
    }

    /** 사용자 추가 정보 삽입 */
    public function insertUserExtraInfo($param){
        $query = "INSERT INTO  tb_user_extra SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , mod_time = now() ";
        $query .= " , reg_time = now() ";
        return $this->insertQuery($query,$param);
    }


    /** 사용자 추가 정보 가져오기 */
    public function getUserExtraInfo($user_no){
        $query = "SELECT 
                    ui.user_admin_memo,
                    ui.user_admin_adres,
                    uie.*  
                  FROM 
                  tb_user_info as ui 
                  LEFT JOIN
                  tb_user_extra as uie
                  ON ui.user_no = uie.user_no
                  WHERE 
                  ui.user_no = ?
                  ";
        return $this->getRowArray($query,[$user_no]);
    }

    /** 사용자 추가 정보 확인 용 */
    public function getUserExtraCount($user_no){
        $query = "SELECT COUNT(*) cnt FROM tb_user_extra WHERE user_no = ? ";
        return $this->getOneField($query,"cnt",[$user_no]);
    }

    /** 회원 정보 임시 저장 */
    public function userInfoTempSave($param){
        $query = "INSERT INTO tb_user_info_temp SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , reg_time = now() ";
        return $this->insertQuery($query,$param);
    }

    /** 회원 임시 정보 불러오기 */
    public function getTempUserInfo($temp_user_no){
        $query = "SELECT * FROM tb_user_info_temp WHERE temp_no = ?";
        return $this->getRowArray($query,[$temp_user_no]);
    }

    /** 회원 임시 정보 삭제 */
    public function deleteTempUserData($temp_user_no){
        $query = "DELETE FROM tb_user_info_temp WHERE temp_no =?";
        return $this->executeQuery($query,[$temp_user_no]);
    }

    /** 사용자 정보 가져오기 - user_no  */
    function getUserInfoByUserNo($user_no){
        $query = "SELECT 
                      tui.*
                      ,tue.card_nm
                      ,tue.card_no
                      ,tue.account_nm
                      ,tue.account_no
                      ,tue.financial_income
                      ,tue.unit_business_income
                      ,tue.business_income
                      ,tue.earned_income
                      ,tue.pension_income
                      ,tue.other_income
                      ,tue.tax_basis
                      ,tue.crystal_tax
                      ,tue.extra_memo
                      ,(SELECT MAX(login_date) FROM tb_user_login WHERE user_no=tui.user_no) login_date
                      ,tai.name adm_name
                      FROM
                      tb_user_info as tui
                      LEFT JOIN
                      tb_user_extra AS tue
                      ON tue.user_no = tui.user_no
                      LEFT JOIN
                      tb_admin_info tai
                      ON tui.mng_emp_no=tai.adm_no
                  WHERE tui.user_no = ?
                  AND tui.del_yn = 'N'
                  ";
        return $this->getRowArray($query,[$user_no]);
    }


    /** 사용자 정보 업데이트 */
    function updateUserInfo($param ,$user_no){
        $query = "UPDATE tb_user_info SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , mod_time = now() ";
        $query .= " WHERE user_no = ?";

        $param[] = $user_no;
        $result = $this->executeQuery($query,$param);
        return $result;
    }

    /** 사용자 추가 정보 업데이트 */
    function updateUserExtraInfo($param ,$user_no){
        $query = "UPDATE tb_user_extra SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , mod_time = now() ";
        $query .= " WHERE user_no = ?";

        $param[] = $user_no;
        $result = $this->executeQuery($query,$param);
        return $result;
    }


    /** 사용자 리스트 업로드한 사용자 정보 */
    public function getUserListByUserNumsTemp($group_no){
        $query = "SELECT * FROM tb_user_info_multi_temp WHERE group_no = ? ORDER BY temp_no ASC";
        return $this->getListArray($query,[$group_no]);
    }

    /** 사용자 리스트 업로드한 사용자 정보 */
    public function getUserListByUserNums($userNums){
        $query = "SELECT * FROM tb_user_info WHERE user_no in ? ORDER BY user_no ASC";
        return $this->getListArray($query,[$userNums]);
    }

    /** 중복되는 이메일 가져오기 */
    public function getUserListByUserEmail($emails){
        $query = "SELECT email_adres FROM tb_user_info WHERE email_adres in ? AND del_yn ='N' ORDER BY user_no ASC";
        return $this->getListArray($query,[$emails]);
    }

    /** 중복되는 휴대폰 가져오기 */
    public function getUserListByUserMobilNo($mobilNos){
        $query = "SELECT mobilno FROM tb_user_info WHERE mobilno in ? AND del_yn='N' ORDER BY user_no ASC";
        return $this->getListArray($query,[$mobilNos]);
    }

    /** 중복되는 사업자 전화번호 가져오기 */
    public function getUserListByUserTelNo($telnos){
        $query = "SELECT telno FROM tb_user_info WHERE telno in ? and del_yn ='N' ORDER BY user_no ASC";
        return $this->getListArray($query,[$telnos]);
    }

    /** 회원 리스트 카운트 */
    public function getUserCount($searchQuery){
        $query =  "SELECT count(*) as cnt FROM (
                      SELECT
                          tui.*
                      FROM tb_user_info AS tui
                          LEFT JOIN
                          tb_tax_order AS tto
                          ON tto.user_no = tui.user_no";
        if($searchQuery!=""){
            $query .= " WHERE tui.del_yn = 'N' AND ";
            $query .= $searchQuery;
        }
        $query .=" GROUP BY tui.user_no
                  ) as allCnt ";
        return $this->getOneField($query,"cnt");
    }

    /** 회원 리스트 */
    public function getUserList($searchQuery , $sLimit, $eLimit , $sort = "DESC", $mode=''){
        $query = "SELECT
                        tui.*,
                        COUNT(tto.or_no) AS serviceCnt,
                        tai.name as adm_name,
                        tue.earned_income,
                        tue.tax_basis,
                        tue.crystal_tax
                    FROM
                        tb_user_info AS tui
                    LEFT JOIN
                        tb_tax_order AS tto
                    ON tto.user_no = tui.user_no
                    LEFT JOIN 
                        tb_user_extra AS tue
                    ON tue.user_no = tui.user_no
                    LEFT JOIN
                        tb_admin_info AS tai
                    ON tui.mng_emp_no = tai.adm_no
                    ";
        if($searchQuery!=""){
            $query .= " WHERE tui.del_yn ='N' AND ";
            $query .= $searchQuery;
        }
        $query .=" GROUP BY tui.user_no
                    ORDER BY user_no " .$sort;
        if($mode == "no_limit") {
            return $this->getListArray($query);
        } else {
            $query .= " LIMIT ? , ? ";
            return $this->getListArray($query,[$sLimit,$eLimit]);
        }
    }

    /** 회원 리스트 */
    public function getCheckUserList($searchQuery){
        $query = "SELECT tui.*,
                        tue.earned_income,
                        tue.tax_basis,
                        tue.crystal_tax
                    FROM tb_user_info AS tui
                    LEFT JOIN
                        tb_user_extra AS tue
                    ON tue.user_no = tui.user_no
                    ";
        if($searchQuery!=""){
            $query .= " WHERE tui.del_yn ='N' AND ";
            $query .= $searchQuery;
        }
        return $this->getListArray($query);
    }

    /** 담당자 배정 히스토리 로그 */
    function adminAssignHistoryInsert($param){
        $query = "INSERT INTO  tb_user_emp_assign_history SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , mod_time = now() ";
        $query .= " , reg_time = now() ";
        return $this->insertQuery($query,$param);
    }

    /** 사용자 디바이스 정보 */
    function getUserDeviceInfo($user_no){
        $query = "SELECT * FROM tb_user_device_reg_ptcl WHERE user_no = ? AND del_yn = 'N'";
        return $this->getListArray($query,[$user_no]);
    }
    
    /** 사용자 디바이스 정보 업데이트 */
    function deviceUpdate($param , $idx){
        $query = "UPDATE tb_user_device_reg_ptcl SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , mod_time = now() ";
        $query .= " WHERE idx = ?";
        $param[] = $idx;
        $result = $this->executeQuery($query,$param);
        return $result;
    }

    /** 아이디 찾기 */
    function findMyId($searchQuery ,$param){
        $query = "SELECT email_adres FROM tb_user_info WHERE  del_yn = 'N' AND user_stat_code !='탈퇴' ".$searchQuery;
        return $this->getOneField($query,'email_adres',$param);
    }

    /** 이메일 인증 로그 삽입 */
    function insertEmailAuthLog($param){
        $query = "INSERT INTO  tb_user_email_crtfc_log SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , reg_time = now() ";
        return $this->insertQuery($query,$param);
    }

    /** 이메일 인증 로그 수정 */
    function emailCrtfcUpdate($yn , $uniqueVal,$type){
        $query = "UPDATE  tb_user_email_crtfc_log SET 
                  email_crtfc_yn = ? 
                  ,authkey_issue_time = now() ";
        if($type!='join') {
            $query .=" WHERE user_no = ? ";
        }else{
            $query .=" WHERE email_adres = ? ";
        }
        $query .=" AND crtfc_type =?
                  AND email_crtfc_yn != 'Y'
                  AND email_crtfc_yn != 'D' ";
        return $this->executeQuery($query, [$yn, $uniqueVal, $type]);
    }

    /** 이메일 유효 체크 */
    function getEmailAuthLastLog($authKey,$type){
        $query = "SELECT * FROM tb_user_email_crtfc_log WHERE crtfc_type = ? AND email_authkey =? AND email_crtfc_yn = 'N' ORDER BY reg_time DESC ";
        return $this->getRowArray($query,[$type,$authKey]);
    }

    /** 이메일 인증 체크 */
    public function getEmailAuthrizedCheck($email,$type){
        $query = "SELECT * FROM tb_user_email_crtfc_log WHERE crtfc_type = ? AND email_adres =? AND email_crtfc_yn = 'Y' AND reg_time >=  (NOW() - INTERVAL 1 DAY) ORDER BY reg_time DESC ";
        return $this->getRowArray($query,[$type,$email]);
    }

    /** 사용자 기기정보 등록 */
    function insertDeviceInfo($param){
        $query = "INSERT INTO  tb_user_device_reg_ptcl SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , reg_time = now() ";
        $query .= " , mod_time = now() ";
        $query .= " , login_date = now() ";
        $query .= " ON DUPLICATE KEY UPDATE mod_time = now() , user_no = '".$param['user_no']."' , push_token = '".$param['push_token']."'";
        return $this->insertQuery($query,$param);
    }

    /** 기기 로그인 처리 */
    function updateDeviceLoginDate($user_no,$device_unique_id){
        $query = "UPDATE tb_user_device_reg_ptcl SET login_date = now() WHERE user_no = ? AND device_uniq_id = ? ";
        return $this->executeQuery($query,[$user_no,$device_unique_id]);
    }

    /** 기기에 매핑된 사용자 번호 */
    function getDeviceUserNo($deviceUniqueId){
        $query = "SELECT user_no FROM tb_user_device_reg_ptcl WHERE device_uniq_id = ?";
        return $this->getOneField($query,'user_no',[$deviceUniqueId]);
    }

    /** 보관함 기타자료 카운트 */
    public function getUserUploadFileCount($searchQuery)
    {
        $query = "SELECT count(*) as cnt
                  FROM tb_receipt_file
                 ";
        if($searchQuery!=""){
            $query .= " WHERE ";
            $query .= $searchQuery;
            $query .= " AND  del_yn ='N' ";
        } else {
            $query .= " WHERE  del_yn ='N' ";
        }

        return $this->getOneField($query,"cnt");
    }

    /** 보관함 기타자료 카운트 */
    public function getUserUploadFileList($searchQuery , $sLimit, $eLimit , $sort = "DESC")
    {
        $query = "SELECT *
                         , DATE_FORMAT(reg_time, '%Y.%m.%d %H:%i ') AS reg_date
                         , DATE_FORMAT(DATE_ADD(reg_time, INTERVAL 5 YEAR), '%Y.%m.%d') AS del_date
                         , DATEDIFF(DATE_ADD(reg_time, INTERVAL 5 YEAR), NOW()) AS del_date_count
                         , IF(now() <= date_add(reg_time, interval +6 day), 'Y', 'N') AS new_show
                         , IF(file_check_count > 0, '확인', '미확인') AS file_check
                         , (SELECT name FROM tb_admin_info WHERE adm_no = rf.adm_no) AS admin_name
                  FROM   tb_receipt_file rf
                 ";
        if($searchQuery!=""){
            $query .= " WHERE ";
            $query .= $searchQuery;
            $query .= " AND  del_yn ='N' ";
        } else {
            $query .= " WHERE  del_yn ='N' ";
        }
        $query .=" ORDER BY reg_time " .$sort. "
                    LIMIT ? , ? ";
        return $this->getListArray($query,[$sLimit,$eLimit]);
    }

    /** 보관함 메모명 수정 */
    public function setUserUploadEdit ($idx, $file_memo)
    {
        $query = "UPDATE tb_receipt_file
                  SET    file_memo = '".$file_memo."',
                         mod_time = now()
                  WHERE  idx = ".$idx;
        $result = $this->executeQuery($query,[$idx,$file_memo]);
        return $result;
    }

    /** 기타자료 보관함 삭제 */
    public function setUserUploadDelete ($idx)
    {
        $query = "UPDATE tb_receipt_file
                  SET    del_yn = 'Y',
                         mod_time = now()
                  WHERE  idx = ".$idx;

        $result = $this->executeQuery($query,$idx);
        return $result;
    }

    /** 기타자료 보관함 조회 증가 */
    public function setUserUploadCount ($idx)
    {
        $query = "UPDATE tb_receipt_file
                  SET    file_check_count = (file_check_count + 1),
                         mod_time = now()
                  WHERE  idx = ".$idx;

        $result = $this->executeQuery($query,$idx);
        return $result;
    }

     /** 신고결과 보관함 카운트 */
    public function getReportResultFileCount($searchQuery)
    {
        $query = "SELECT count(*) as cnt
                  FROM tb_result_file
                 ";
        if($searchQuery!=""){
            $query .= " WHERE ";
            $query .= $searchQuery;
            $query .= " AND  del_yn ='N' ";
        } else {
            $query .= " WHERE  del_yn ='N' ";
        }

        return $this->getOneField($query,"cnt");
    }

    /** 신고결과 보관함 리스트 */
    public function getReportResultFileList($searchQuery , $sLimit, $eLimit , $sort = "DESC")
    {
        $query = "SELECT *
                         , DATE_FORMAT(reg_time, '%Y.%m.%d %H:%i ') AS reg_date
                         , DATE_FORMAT(DATE_ADD(reg_time, INTERVAL 5 YEAR), '%Y.%m.%d') AS del_date
                         , DATEDIFF(DATE_ADD(reg_time, INTERVAL 5 YEAR), NOW()) AS del_date_count
                         , IF(now() <= date_add(reg_time, interval +6 day), 'Y', 'N') AS new_show
                         , IF(file_check_count > 0, '확인', '미확인') AS file_check
                  FROM   tb_result_file rf
                 ";
        if($searchQuery!=""){
            $query .= " WHERE ";
            $query .= $searchQuery;
            $query .= " AND  del_yn ='N' ";
        } else {
            $query .= " WHERE  del_yn ='N' ";
        }
        $query .=" ORDER BY reg_time " .$sort. "
                    LIMIT ? , ? ";
        //echo $query;
        return $this->getListArray($query,[$sLimit,$eLimit]);
    }

    /** 신고결과 보관함 삭제 */
    public function setReportResultDelete ($idx)
    {
        $query = "UPDATE tb_result_file
                  SET    del_yn = 'Y',
                         mod_time = now()
                  WHERE  rs_no = ".$idx;

        $result = $this->executeQuery($query,$idx);
        return $result;
    }

    /** 신고결과 보관함 조회 증가 */
    public function setReportResultCount ($idx)
    {
        $query = "UPDATE tb_result_file
                  SET    file_check_count = (file_check_count + 1),
                         mod_time = now()
                  WHERE  rs_no = ".$idx;

        $result = $this->executeQuery($query,$idx);
        return $result;
    }

    /** 전화번호 중복 체크 */
    public function dupCheckNumbers($type,$nums){
        $query = "SELECT count(user_no) as cnt FROM tb_user_info WHERE del_yn='N' AND ".$type." = '".$nums."'";
        return $this->getOneField($query,'cnt');
    }

    /** 관리자 계정 및 서비스 승인 요청 로그 삽입 */
    public function insertSendAuthmailLog($param){
        $query = "INSERT INTO tb_send_authmail_log SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , reg_time = now() ";
        return $this->insertQuery($query,$param);
    }

    /** 관리자 계정 및 서비스 승인 요청 로그 수정 */
    public function updateSendAuthmailLog ($param, $idx)
    {
        $query = "UPDATE tb_send_authmail_log SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " WHERE sa_no = ? ";

        $param[] = $idx;
        return $this->executeQuery($query,$param);
    }

    /** 관리자 계정 및 서비스 승인 요청 로그 삭제 */
    public function deleteSendAuthmailLog ($idx)
    {
        $query = "DELETE FROM tb_send_authmail_log WHERE sa_no = ? ";
        return $this->executeQuery($query,$idx);
    }

    /** user_no로 승인되지 않은 승인 요청 관계 정보 추출 */
    public function getAuthmailRelationByUserNo ($userNo, $sqlWhere='', $limit='')
    {
        $query = "SELECT * FROM rela_authmail_member WHERE user_no = ? " . $sqlWhere . " ORDER BY reg_time DESC";
        if($limit != '') {
            $query .= " LIMIT " . $limit;
        }
        return $this->getRowArray($query,[$userNo]);
    }

    /** sa_no로 승인되지 않은 승인 요청 관계 정보 추출 */
    public function getAuthmailRelationBySaNo ($saNo, $sqlWhere='', $limit='')
    {
        $query = "SELECT * FROM rela_authmail_member WHERE sa_no = ? " . $sqlWhere . " ORDER BY reg_time DESC";
        if($limit != '') {
            $query .= " LIMIT " . $limit;
        }
        return $this->getRowArray($query,[$saNo]);
    }

    /** 승인 요청 관계 정보 삽입 */
    public function insertSendAuthmailRelation($param){
        $query = "INSERT INTO rela_authmail_member SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , reg_time = now() ";
        return $this->insertQuery($query,$param);
    }

    /** 승인 요청 관계 정보 수정 */
    public function updateSendAuthmailRelation($param, $saNo, $userNo){
        $query = "UPDATE rela_authmail_member SET ";
        $updateFiled = array();
        foreach($param as $key => $val){
            $updateFiled[] = $key." = ? ";
        }
        $query .= implode(",",$updateFiled);
        $query .= " , auth_time = now() ";
        $query .= " WHERE sa_no = ?
                        AND user_no = ?";
        $param['sa_no'] = $saNo;
        $param['user_no'] = $userNo;

        return $this->executeQuery($query,$param);
    }

    public function mulitCancel($groupNo){
        $query = "DELETE FROM tb_user_info_multi_temp WHERE group_no = ?";
        return $this->executeQuery($query,[$groupNo]);
    }

}
