<?php
/**
 * Created by PhpStorm.
 * User: WebDev04
 * Date: 2018-09-21
 * Time: 오전 10:21
 */
class DefaultModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function LoginCheck($param){
        $query = "SELECT * FROM 
                    admin  
                  WHERE admin_id = ? 
                  AND password = password(?) ";
        return $this->getRowArray($query,$param);
    }
    function getAdmin(){
        $query = "SELECT * FROM admin ";
        return $this->getRowArray($query);
    }

    function saveAdmin($param){
        $query = "UPDATE admin SET ";
        $addStringBuf = array();
        foreach ($param as $key => $val){
            if($key=="password"){
                $addStringBuf[] = $key ." = password('". $val."') ";
            }else{
                $addStringBuf[] = $key ." = '". $val."' ";
            }
        }
        $query .= implode(",",$addStringBuf);
        return $this->executeQuery($query);
    }

    function monitorSave($param,$no){
        $query = "UPDATE monitorSetup SET ";
        $addStringBuf = array();
        foreach ($param as $key => $val){
            $addStringBuf[] = $key ." = '". $val."' ";
        }
        $query .= implode(",",$addStringBuf);
        $query .= " WHERE no = ".$no;
        return $this->executeQuery($query);
    }

    function removeMenu($monitorNo){
        $query= "DELETE FROM menuText WHERE monitorNum = ?";
        return $this->executeQuery($query,[$monitorNo]);
    }

    function insertMenu($param){
        $query = "INSERT INTO menuText SET ";
        $addStringBuf = array();
        foreach ($param as $key => $val){
            $addStringBuf[] = $key ." = '". $val."' ";
        }
        $query .= implode(",",$addStringBuf);
        return $this->executeQuery($query);
    }

    function getMonitorSet($monitorNo){
        $query = "SELECT * FROM monitorSetup WHERE no = ?";
        return $this->getRowArray($query,[$monitorNo]);
    }

    function getMenuText($monitorNo){
        $query = "SELECT * FROM menuText WHERE monitorNum = ?";
        return $this->getListArray($query,[$monitorNo]);
    }

    function getFontList(){
        $query= "SELECT * FROM font ORDER BY idx ASC";
        return $this->getListArray($query);
    }

}
