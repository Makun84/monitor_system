
<section class="content-header">
    <h1>
        관리자 정보 변경
<!--        <small>Control panel</small>-->
    </h1>
<!--    <ol class="breadcrumb">-->
<!--        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>-->
<!--        <li class="active">Dashboard</li>-->
<!--    </ol>-->
</section>
<section class="content">
    <div class="col-lg-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">관리자 정보</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" id="admin_frm">
                <div class="box-body">
                    <div class="form-group">
                        <label for="admin_id">계정</label>
                        <input type="text" class="form-control" id="admin_id" name="admin_id" placeholder="계정" value="<?=$admin['admin_id']?>" maxlength="10">
                    </div>
                    <div class="form-group">
                        <label for="password">비밀번호</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="비밀번호" maxlength="30">
                    </div>
                    <div class="form-group">
                        <label for="password">비밀번호 확인</label>
                        <input type="password" class="form-control" id="password_chk" placeholder="비밀번호 확인"  maxlength="30">
                    </div>
                    <div class="form-group">
                        <label for="admin_name">이름</label>
                        <input type="text" class="form-control" id="admin_name" name="admin_name" placeholder="이름"  value="<?=$admin['admin_name']?>"  maxlength="30">
                    </div>
                    <div class="form-group">
                        <label for="shop_name">상호명</label>
                        <input type="text" class="form-control" id="shop_name" name="shop_name" placeholder="상호명"  value="<?=$admin['shop_name']?>"  maxlength="30">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                    <button type="submit" class="btn btn-primary">저장</button>
                    <button type="button" class="btn btn-danger">취소</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script>
    $("#admin_frm").on("submit",function () {
        if($("#admin_id").val() == ""){
            alert("계정을 입력해주세요.");
            return false;
        }
        if($("#password").val() != ""){
            if($("#password_chk").val() != $("#password").val()){
                alert("비밀번호 가 일치하지 않습니다.\n비밀번호 확인을 위해 다시한번 입력해주세요");
                return false;
            }
        }
        if($("#admin_name").val()==""){
            alert("이름을 입력해주세요");
            return false;
        }
        if($("#shop_name").val()==""){
            alert("상호명을 입력해주세요.");
            return false;
        }
        var param = $("#admin_frm").serializeObject();

        var getResult = getJson("/Admin/SaveAdmin",param,false);
        if(getResult.result==true){
            alert("저장 되었습니다.");
            location.reload();
        }else {
            alert("시스템 장애가 발생했습니다.");
        }
        return false;
    });
</script>