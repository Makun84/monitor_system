<style>
    .menuArea{
        border: 1px solid #0a0a0a;
        border-radius : 10px;
        margin-top: 50px;
        margin-bottom: 50px;
        max-height: 985px;
        overflow: hidden;
    }

    .menuAlignCenter{
        margin-left: auto;
        margin-right: auto;
    }

    .menuAlignLeft{
        margin-left: 50px;
        margin-right: auto;
    }

    .menuAlignRight{
        margin-right: 50px;
        margin-left: auto;
    }

    .size100p{
        width: 1800px;
    }
    .size50p{
        width: 900px;
    }
    .size30p{
        width: 600px;
    }

    .notMenu100Left{
        display: inline-block;
        float: left;
    }

    .notMenu100Right{
        display: inline-block;
        float: right;
    }

    .useBgMenu{
        <?=($menuBgColor!="")?$menuBgColor:""?>
    }

    .defaultTb{
        text-align: center;
        width: 100%;
    }

    #videoIncluded{
        position: absolute;
        margin-top: 140px;
        height: 800px;
    }

    /* 메뉴 사이즈 50 */
    .videoSize50p{
        width: 800px;
    }
    /* 메뉴 사이즈 30 */
    .videoSize30p{
        width: 1050px;
    }

    .videoMargin1050{
        margin-left: 1050px;
    }
    .videoMargin50{
        margin-left: 50px;
    }
    .videoMargin800{
        margin-left: 800px;
    }

    .mgLeft30{
        margin-left: 30px;
    }

    .mgRight30{
        margin-right: 30px;
    }
</style>
<?php
$videoClass="";
$videoClass .= " videoSize".$menuWidth."p";
if($menuWidth=="50"){
    if($menuAlign=="Left"){
        $videoClass .=" videoMargin1050";
    }else if($menuAlign=="Right"){
        $videoClass .=" videoMargin50";
    }
}else if($menuWidth=="30"){
    if($menuAlign=="Left"){
        $videoClass .=" videoMargin800";
    }else if($menuAlign=="Right"){
        $videoClass .=" videoMargin50";
    }
}


if($includeVideo){
    ?>
    <video id="videoIncluded" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0" class="<?=$videoClass?>">
        <source src="<?=$info['bg_mov']?>">
    </video>
    <?php
}
?>


<?php

$addClass = "";
if($menuBgColor!="") {
    $addClass .= "useBgMenu";
}
$addClass .= " size".$menuWidth."p";
$addClass .= " menuAlign".$menuAlign;
if($pageEffect){
    $addClass .= " item";
}

if($pageEffect==false && $includeVideo==false && $menuWidth!="100"){
    $addClass .= " notMenu100".$menuAlign;
    $addClass .= " mg".$menuAlign.$menuWidth;
}

echo ($pageEffect)?"<div class=\"owl-carousel\">":"";
foreach ($menu as $page) {
    if($page != false) {
            ?>
        <div class="menuArea <?=$addClass?>">
            <table class="defaultTb">
            <?php
            foreach ($page as $me) {
                if( $me['menuName'] !='' && $me['menuType'] != '') {
                    ?>
                    <tr class="<?= $me['menuType'] ?>">
                        <?php if ($me['menuType'] == "title") { ?>
                            <td colspan="2"><?= $me['menuName'] ?></td>
                        <?php } else { ?>
                            <td><?= $me['menuName'] ?></td>
                            <td><?= ($me['menuPrice'] != '') ? number_format($me['menuPrice']) : '' ?>원</td>
                        <?php } ?>
                    </tr>
                    <?php
                }
            }
        ?>
            </table>
        </div>
        <?php
    }
}
echo ($pageEffect)?"</div>":"";
if($pageEffect){
    ?>
    <script>
        $('.owl-carousel').owlCarousel({
            margin: 10,
            items: 1,
            nav: false,
            loop: true,
            animateOut: 'slideOutDown',
            animateIn: 'flipInX',
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:false
        });
    </script>
    <?php
}
?>
