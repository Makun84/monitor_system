<section class="content-header">
    <h1>
        모니터 No <?=$num?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <form name="moniterFrm" id="monitorFrm" method="post">
        <input type="hidden" name="no" value="<?=$num?>"  enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">배경화면 설정</h3>
                </div>
                <div class="box-body">
                    <!-- radio -->
                    <div class="form-group">
                        <label>
                            <input type="radio" name="templet_type" value="1" class="flat-red" <?=($info['templet_type']==1)?"checked":""?>>
                            배경 화면 이미지 설정
                        </label>
                        <label>
                            <input type="radio" name="templet_type" value="2" class="flat-red" <?=($info['templet_type']==2)?"checked":""?>>
                            배경 화면 동영상 설정
                        </label>
                        <label>
                            <input type="radio" name="templet_type" value="3" class="flat-red"  <?=($info['templet_type']==3)?"checked":""?>>
                            동영상 포함 [메뉴 사이즈 100% 및 가운데 정렬 사용 불가]
                        </label>
                    </div>
                    <div class="form-group">
                        <label>배경 이미지</label>
                        <input type="file" name="bg_img" id="bg_img" class="form-control" accept="image/*" <?=($info['templet_type']==2)?"disabled":""?>>
                        <?=($info['bg_img']!="")?"<a href='".$info['bg_img']."'>[배경이미지]</a><br/>":""?>
                        <label>영상</label>

                        <input type="file" name="bg_mov" id="bg_mov" class="form-control" accept="video/*" <?=($info['templet_type']==1)?"disabled":""?>>
                        <?=($info['bg_mov']!="")?"<a href='".$info['bg_mov']."'>[영상]</a><br/>":""?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">메뉴 정렬</h3>
                </div>
                <div class="box-body">
                    <!-- radio -->
                    <div class="form-group">
                        <label>
                            <input type="radio" name="text_loc" value="1" class="flat-red" <?=($info['text_loc']==1)?"checked":""?> <?=($info['templet_type']==3)?"disabled":""?>>
                            사이즈 100%
                        </label>

                        <label>
                            <input type="radio" name="text_loc" value="2" class="flat-red" <?=($info['text_loc']==2)?"checked":""?>>
                            사이즈 50%
                        </label>
                        <label>
                            <input type="radio" name="text_loc" value="3" class="flat-red" <?=($info['text_loc']==3)?"checked":""?>>
                            사이즈 30%
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" name="text_type"  value="1" class="flat-red" <?=($info['text_type']==1 && $info['text_loc']!=1)?"checked":""?> <?=($info['text_loc']==1)?"disabled":""?>>
                            왼쪽 정렬
                        </label>

                        <label>
                            <input type="radio" name="text_type" value="2" class="flat-red" <?=($info['templet_type']==3)?"disabled":""?> <?=($info['text_type']==2||$info['text_loc']==1)?"checked":""?>>
                            가운데 정렬
                        </label>

                        <label>
                            <input type="radio" name="text_type" value="3" class="flat-red" <?=($info['text_type']==3  && $info['text_loc']!=1)?"checked":""?> <?=($info['text_loc']==1)?"disabled":""?>>
                            오른쪽 정렬
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" name="pageEffect"  value="1" class="flat-red" <?=($info['pageEffect']==1)?"checked":""?>>
                            페이지 전환 효과 사용함
                        </label>

                        <label>
                            <input type="radio" name="pageEffect" value="2" class="flat-red" <?=($info['pageEffect']==2)?"checked":""?>>
                            페이지 전환 효과 사용안함
                        </label>
                        <br/>
                        <span class="text-danger">
                            [메뉴 1PAGE 이상 등록시 전환효과를 사용여부를 체크 합니다.] <br/>
                            ex) 2PAGE 등록 사이즈 50% 전환 효과 사용 X 하나의 화면에서 노출 됩니다.<br/>
                            3PAGE 등록 사이즈 30% 전환 효과 사용 X 하나의 화면에서 노출 됩니다.<br/>
                            전환 효과 사용시 각각의 화면 에서 변환 동작 합니다.<br/>
                        </span>
                    </div>
                </div>
                <div class="form-group" style="padding-left: 10px;">
                    <div class="row">
                    <div class="col-lg-3">
                        <div class="input-group">
                            <span style="border-radius: 0;border-color: #d2d6de;background-color: #fff;">
                               <label>
                                   메뉴 영역 배경사용 [반투명으로 적용 됩니다]
                                   <input type="checkbox" id="menuBgUse" name="menuBgUse" value="Y" <?=($info['menuBgUse']=="Y")?"checked":""?>>
                               </label>
                            </span>
                        </div>
                        <div class="input-group colorPicker">
                            <input type="text" class="form-control" name="menuBgColor" id="menuBgColor" <?=($info['menuBgUse']=="Y")?"":"disabled"?> value="<?=$info['menuBgColor']?>" placeholder="색상코드를 입력하세요">
                            <div class="input-group-addon">
                                <i></i>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <label style="margin-left: 10px;">
                            제목 폰트
                            <select name="titleFont" id="titleFont" class="form-control">
                                <option value=""  fm="">==기본==</option>
                                <?php foreach ($fonts as $font) {
                                    $selected = ($info['titleFont']==$font['idx'])?"selected":"";
                                    ?>
                                <option value="<?=$font['idx']?>" style="font-family:<?=$font['FontFamily']?>;" fm="<?=$font['FontFamily']?>" <?=$selected?>><?=$font['FontName']?></option>
                                <?php }?>
                            </select>
                        </label>
                        <label style="margin-left: 10px;">
                            제목 크기
                            <select name="titleSize" id="titleSize" class="form-control">
                                <option value="14px">==14px[기본]==</option>
                                <option value="30px" <?=($info['titleSize']=="30px")?"selected":""?>>30px</option>
                                <option value="40px" <?=($info['titleSize']=="40px")?"selected":""?>>40px</option>
                                <option value="50px" <?=($info['titleSize']=="50px")?"selected":""?>>50px</option>
                                <option value="80px" <?=($info['titleSize']=="80px")?"selected":""?>>80px</option>
                                <option value="100px" <?=($info['titleSize']=="100px")?"selected":""?>>100px</option>
                            </select>
                        </label>
                        <label style="margin-left: 10px;">
                            제목 색상
                            <div class="input-group colorPicker">
                                <input type="text" class="form-control" name="titleColor" id="titleColor" value="<?=$info['titleColor']?>"  placeholder="색상코드를 입력하세요">
                                <div class="input-group-addon">
                                    <i></i>
                                </div>
                            </div>
                        </label>
                    </div>
                    <span id="titleFontShow" style="margin-left:10px;">제목 폰트 [가나다라마바사아자차카타파하 1234567890]</span>
                </div>
                <div class="form-group">
                    <div class="input-group">
                    <label style="margin-left: 10px;">
                        메뉴 폰트
                        <select name="menuFont" id="menuFont" class="form-control">
                            <option value="" fm="">==기본==</option>
                            <?php foreach ($fonts as $font) {
                                $selected = ($info['menuFont']==$font['idx'])?"selected":"";
                                ?>
                                <option value="<?=$font['idx']?>" style="font-family:<?=$font['FontFamily']?>;" fm="<?=$font['FontFamily']?>" <?=$selected?>><?=$font['FontName']?></option>
                            <?php }?>
                        </select>
                    </label>
                    <label style="margin-left: 10px;">
                        메뉴 크기
                        <select name="menuSize" id="menuSize" class="form-control">
                            <option value="14px">==14px[기본]==</option>
                            <option value="30px" <?=($info['menuSize']=="30px")?"selected":""?>>30px</option>
                            <option value="40px" <?=($info['menuSize']=="40px")?"selected":""?>>40px</option>
                            <option value="50px" <?=($info['menuSize']=="50px")?"selected":""?>>50px</option>
                            <option value="80px" <?=($info['menuSize']=="80px")?"selected":""?>>80px</option>
                            <option value="100px" <?=($info['menuSize']=="100px")?"selected":""?>>100px</option>
                        </select>
                    </label>
                    <label style="margin-left: 10px;">
                        메뉴 색상
                        <div class="input-group colorPicker">
                            <input type="text" class="form-control" name="menuColor" id="menuColor" value="<?=$info['menuColor']?>"  placeholder="색상코드를 입력하세요">
                            <div class="input-group-addon">
                                <i></i>
                            </div>
                        </div>
                    </label>
                    </div>
                    <span id="menuFontShow" style="margin-left: 10px;">메뉴 폰트 [가나다라마바사아자차카타파하 1234567890]</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">메뉴판</h3>
                </div>

                <div class="box-body">
                    <div class="col-lg-4 col-xs-4 col-sm-4 text-center">
                        PAGE 1 <button type="button" class="btn btn-primary AddBtn" pageNo="1">메뉴 및 제목 추가</button>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-sm-4 text-center">
                        PAGE 2 <button type="button" class="btn btn-primary AddBtn" pageNo="2">메뉴 및 제목 추가</button>
                    </div>
                    <div class="col-lg-4 col-xs-4 col-sm-4 text-center">
                        PAGE 3 <button type="button" class="btn btn-primary AddBtn" pageNo="3">메뉴 및 제목 추가</button>
                    </div>
                    <table class="table " id="text_row_area_1" style="width: 33%;display: inline-block;float: left;">
                        <tr class="no_row <?=($page1Menu!=false)?"hidden":""?>" ><td colspan='7' class='text-center'>등록된 메뉴가 없습니다.</td></tr>
                        <?php
                        if($page1Menu!=false){
                                foreach ($page1Menu as $menu1){
                                    ?>
                                    <tr class="text_row">
                                        <th>타입</th>
                                        <td>
                                            <input type="hidden" name="pageNo[]" value="1">
                                            <select name="menuType[]" class="form-control text_type_sel">
                                                <option value="">==선택해주세요==</option>
                                                <option value="title" <?=($menu1['menuType']=="title")?"selected":""?>>제목</option>
                                                <option value="menu" <?=($menu1['menuType']=="menu")?"selected":""?>>메뉴</option>
                                            </select>
                                        </td>
                                        <th><?=($menu1['menuType']=="title")?"제목":"메뉴"?></th>
                                        <td>
                                            <input type="text" name="menuName[]" class="form-control" value="<?=$menu1['menuName']?>">
                                        </td>
                                        <th><?=($menu1['menuType']=="title")?"":"가격"?></th>
                                        <td><?=($menu1['menuType']=="title")?"<input type='number' name='menuPrice[]' class='form-control hidden'>":"<input type='number' name='menuPrice[]' class='form-control' value='".$menu1['menuPrice']."'>"?></td>
                                        <td><button type="button" class="btn btn-sm btn-danger row_delete" pageNo="1">삭제</button></td>
                                    </tr>
                                    <?php
                                }
                            }
                        ?>
                    </table>
                    <table class="table" style="width: 33%;display: inline-block;float: left;" id="text_row_area_2">
                        <tr class="no_row <?=($page2Menu!=false)?"hidden":""?>" ><td colspan='7' class='text-center'>등록된 메뉴가 없습니다.</td></tr>
                        <?php
                        if($page2Menu!=false){
                            foreach ($page2Menu as $menu2){
                                ?>
                                <tr class="text_row">
                                    <th>타입</th>
                                    <td>
                                        <input type="hidden" name="pageNo[]" value="2">
                                        <select name="menuType[]" class="form-control text_type_sel">
                                            <option value="">==선택해주세요==</option>
                                            <option value="title" <?=($menu2['menuType']=="title")?"selected":""?>>제목</option>
                                            <option value="menu" <?=($menu2['menuType']=="menu")?"selected":""?>>메뉴</option>
                                        </select>
                                    </td>
                                    <th><?=($menu2['menuType']=="title")?"제목":"메뉴"?></th>
                                    <td>
                                        <input type="text" name="menuName[]" class="form-control" value="<?=$menu2['menuName']?>">
                                    </td>
                                    <th><?=($menu2['menuType']=="title")?"":"가격"?></th>
                                    <td><?=($menu2['menuType']=="title")?"<input type='number' name='menuPrice[]' class='form-control hidden'>":"<input type='number' name='menuPrice[]' class='form-control' value='".$menu2['menuPrice']."'>"?></td>
                                    <td><button type="button" class="btn btn-sm btn-danger row_delete" pageNo="2">삭제</button></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>

                    <table class="table" style="width: 33%;display: inline-block;float: left;" id="text_row_area_3">
                        <tr class="no_row <?=($page3Menu!=false)?"hidden":""?>" ><td colspan='7' class='text-center'>등록된 메뉴가 없습니다.</td></tr>
                        <?php
                        if($page3Menu!=false){
                            foreach ($page3Menu as $menu3){
                                ?>
                                <tr class="text_row">
                                    <th>타입</th>
                                    <td>
                                        <input type="hidden" name="pageNo[]" value="3">
                                        <select name="menuType[]" class="form-control text_type_sel">
                                            <option value="">==선택해주세요==</option>
                                            <option value="title" <?=($menu3['menuType']=="title")?"selected":""?>>제목</option>
                                            <option value="menu" <?=($menu3['menuType']=="menu")?"selected":""?>>메뉴</option>
                                        </select>
                                    </td>
                                    <th><?=($menu3['menuType']=="title")?"제목":"메뉴"?></th>
                                    <td>
                                        <input type="text" name="menuName[]" class="form-control" value="<?=$menu3['menuName']?>">
                                    </td>
                                    <th><?=($menu3['menuType']=="title")?"":"가격"?></th>
                                    <td><?=($menu3['menuType']=="title")?"<input type='number' name='menuPrice[]' class='form-control hidden'>":"<input type='number' name='menuPrice[]' class='form-control' value='".$menu3['menuPrice']."'>"?></td>
                                    <td><button type="button" class="btn btn-sm btn-danger row_delete" pageNo="2">삭제</button></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                    </table>
                </div>
            </div>
            <div class="box-footer">
                <button type="button" class="btn btn-default" id="btnCancel">취소</button>
                <button type="button" class="btn btn-danger pull-right" id="btnSave">저장</button>
            </div>
        </div>
    </form>
    </div>
</section>

<script>
    $("input[name=text_loc]").on("change",function () {
        if($("input[name=templet_type]").val()==3){
            $("input[name=text_loc]:eq(0)").prop("disabled",true);
        }
        if ($(this).val() == 1) {
            $("input[name=text_type]").each(function () {
                if ($(this).val() != 2) {
                    $(this).prop("disabled", true);
                } else {
                    $(this).prop("checked", true);
                }
            });

        } else {
            $("input[name=text_type]").each(function () {
                $(this).prop("disabled", false);
            });
            if($("input[name=templet_type]:checked").val()==3){
                $("input[name=text_type]:eq(1)").prop("disabled",true);
                $("input[name=text_loc]:eq(0)").prop("disabled",true);
            }
        }

    });

    $("input[name=templet_type]").on("change",function () {
        $("input[name=text_loc]").prop("disabled",false);
        if($(this).val()==1){
            $("#bg_img").prop("disabled",false);
            $("#bg_mov").prop("disabled",true);
        }else if($(this).val()==2){
            $("#bg_img").prop("disabled",true);
            $("#bg_mov").prop("disabled",false);
        }else{
            $("#bg_img").prop("disabled",false);
            $("#bg_mov").prop("disabled",false);
            $("input[name=text_loc]").each(function () {
                if($(this).val()==1){
                    if($(this).is(":checked")){
                        $("input[name=text_loc]:eq(1)").click();
                        $("input[name=text_type]").each(function () {
                            if($(this).val()==2){
                                if($(this).is(":checked")){
                                    $("input[name=text_type]:eq(0)").click();
                                }
                                $(this).prop("disabled",true);
                            }
                        });
                    }
                    $(this).prop("disabled",true);
                }
            });
        }
    });

    $("#menuBgUse").change(function () {
        if($(this).is(":checked")){
            $("#menuBgColor").prop("disabled",false);
        }else{
            $("#menuBgColor").prop("disabled",true);
        }
    });

    $("#menuColor").on("change",function () {
        var sz = $(this).val();
        $("#menuFontShow").css("color",sz);
    });
    $("#titleColor").on("change",function () {
        var sz = $(this).val();
        $("#titleFontShow").css("color",sz);
    });

    $("#menuSize").on("change",function () {
        var sz = $(this).find("option:selected").val();
        $("#menuFontShow").css("font-size",sz);
    });
    $("#titleSize").on("change",function () {
        var sz = $(this).find("option:selected").val();
        $("#titleFontShow").css("font-size",sz);
    });
    $("#titleFont").on("change",function () {
        var fm = $(this).find("option:selected").attr("fm");
        $("#titleFontShow").css("font-family",fm);
    });
    $("#menuFont").on("change",function () {
        var fm = $(this).find("option:selected").attr("fm");
        $("#menuFontShow").css("font-family",fm);
    });
    $(".AddBtn").on("click",function () {
        var pageNo = $(this).attr("pageNo");
        $("#text_row_area_"+pageNo).find(".no_row").addClass("hidden");
        var newTr = $("<tr class='text_row'>");
        var newTh1 = $('<th>');
        var newTh2 = $('<th>');
        var newTh3 = $('<th>');
        var newTd1 = $("<td>");
        var newTd2 = $("<td>");
        var newTd3 = $("<td>");
        var newTd4 = $("<td>");
        var newDelBtn = $("<button type=\"button\" class=\"btn btn-danger row_delete\" pageNo=\""+pageNo+"\">");
        var newSel = $("<select name=\"menuType[]\" class=\"form-control text_type_sel\">");
        var newOption1 = $("<option>");
        var newOption2 = $("<option>");
        var newOption3 = $("<option>");
        var newInput1 = $("<input type=\"text\" name=\"menuName[]\" class=\"form-control\">");

        newTh1.append("타입");
        newOption1.val("");
        newOption1.text("==선택해주세요==");
        newOption2.val("title");
        newOption2.text("제목");
        newOption3.val("menu");
        newOption3.text("메뉴");
        newSel.append(newOption1);
        newSel.append(newOption2);
        newSel.append(newOption3);
        newTd1.append("<input type=\"hidden\" name=\"pageNo[]\" value=\""+pageNo+"\">");
        newTd1.append(newSel);
        newTh2.append("제목");
        newTd2.append(newInput1);
        newTd3.append("<input type=\"number\" name=\"menuPrice[]\" class=\"form-control hidden\">");
        newTd4.append(newDelBtn);

        newDelBtn.append("삭제");

        newTr.append(newTh1);
        newTr.append(newTd1);
        newTr.append(newTh2);
        newTr.append(newTd2);
        newTr.append(newTh3);
        newTr.append(newTd3);
        newTr.append(newTd4);
        $("#text_row_area_"+pageNo).append(newTr);
    });
    $(document).on("change",".text_type_sel",function () {
        if($(this).val()=="title"){
            $(this).closest("tr").find("th:eq(1)").text("제목");
            $(this).closest("tr").find("th:eq(2)").html('');
            $(this).closest("tr").find("td:eq(2)").html('<input type="number" name="menuPrice[]" class="form-control hidden">');
        }else if($(this).val()=="menu"){
            $(this).closest("tr").find("th:eq(1)").text("메뉴");
            $(this).closest("tr").find("th:eq(2)").html('가격');
            $(this).closest("tr").find("td:eq(2)").html('<input type="number" name="menuPrice[]" class="form-control">');
        }
    });
    $(document).on("click",".row_delete",function () {
        var pageNo = $(this).attr("pageNo");
        $(this).closest("tr").remove();
        if($("#text_row_area_"+pageNo).find(".text_row").length==0 || typeof $("#text_row_area_"+pageNo).find(".text_row") == "undefined"){
            $("#text_row_area_"+pageNo).find(".no_row").removeClass("hidden");
        }
    });

    $(document).ready(function () {
        $('.colorPicker').colorpicker();

        $("#titleFont").find("option").each(function () {
            if($(this).is(":selected")){
                var fm = $(this).attr("fm");
                $("#titleFontShow").css("font-family",fm);
            }
        });

        $("#menuFont").find("option").each(function () {
            if($(this).is(":selected")){
                var fm = $(this).attr("fm");
                $("#menuFontShow").css("font-family", fm);
            }
        });

        $("#titleSize").find("option").each(function () {
            if($(this).is(":selected")){
                var sz = $(this).val();
                $("#titleFontShow").css("font-size",sz);
            }
        });

        $("#menuSize").find("option").each(function () {
            if($(this).is(":selected")){
                var sz = $(this).val();
                $("#menuFontShow").css("font-size",sz);
            }
        });
     });

    $("#btnCancel").on("click",function () {
        location.reload();
    });
    
    $("#btnSave").on("click",function () {
        var form = $("#monitorFrm")[0];
        var formData = new FormData(form);
        var getResult = getJsonFile("/Admin/MonitorSave",formData,false);
        if(getResult.result==true){
            location.reload();
        }
    });
</script>