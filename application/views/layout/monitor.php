<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/asset/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/asset/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/asset/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 3 -->
    <script src="/asset/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="/asset/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.7 -->
    <script src="/asset/bootstrap/dist/js/bootstrap.min.js"></script>

<!--    <script src="/dist/js/adminlte.min.js"></script>-->
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Black+And+White+Picture|Black+Han+Sans|Cute+Font|Do+Hyeon|Dokdo|East+Sea+Dokdo|Gaegu|Gamja+Flower|Gothic+A1|Gugi|Hi+Melody|Jua|Kirang+Haerang|Nanum+Brush+Script|Nanum+Gothic|Nanum+Gothic+Coding|Nanum+Myeongjo|Nanum+Pen+Script|Noto+Sans+KR|Noto+Serif+KR|Poor+Story|Song+Myung|Stylish|Sunflower:300|Yeon+Sung" rel="stylesheet">
    <script src="/dist/common.js"></script>
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="/plugins/owlcarousel/assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/plugins/owlcarousel/assets/css/animate.css">
    <link rel="stylesheet" href="/plugins/owlcarousel/assets/owlcarousel/assets/owl.theme.default.min.css">
    <script src="/plugins/owlcarousel/assets/owlcarousel/owl.carousel.js"></script>
    <style>
        .wrapper {
            width: 1920px;
            height: 1080px;
            padding : 0px;
            border: 0px;
            border: 1px solid #0a0a0a;
            position: absolute;
            overflow: hidden;
        }
        <?php if(($bg_type==1 || $bg_type==3) && isset($bg_img) && $bg_img !=''){ ?>
        body {
            background-image: url("<?=$bg_img?>");
            background-size: cover;
            background-repeat: no-repeat;
        }
        <?php }?>
        .title {
            padding-top: 50px;
        <?php if(isset($titleSize) && $titleSize!=''){ ?>
            font-size: <?=$titleSize?>;
        <?php } ?>
        <?php if(isset($titleFont) && $titleFont!=''){ ?>
            font-family: <?=$titleFont?>;
        <?php } ?>
        <?php if(isset($titleColor) && $titleColor!=''){ ?>
            color: <?=$titleColor?>;
        <?php } ?>
        }

        .title > td{
            padding-top : 30px;
        }
        .menu {
        <?php if(isset($menuSize) && $menuSize!=''){ ?>
            font-size: <?=$menuSize?>;
        <?php } ?>
        <?php if(isset($menuFont) && $menuFont!=''){ ?>
            font-family: <?=$menuFont?>;
        <?php } ?>
        <?php if(isset($menuColor) && $menuColor!=''){ ?>
            color: <?=$menuColor?>;
        <?php } ?>
        }

        <?php if($bg_type==2 && isset($bg_mov) && $bg_mov!=''){ ?>
        #video {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 1920px;
            height: 1080px;
            z-index: -1;
            overflow: hidden;
        }
        <?php }?>
    </style>
</head>
<body>
    <?php if($bg_type==2 && isset($bg_mov) && $bg_mov!=''){ ?>
    <video id="video" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0" >
        <source src="<?=$bg_mov?>">
    </video>
    <?php }?>
    <div class="wrapper">
    {output}
    </div>
</body>
