<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
    public $user;
	public function __construct()
	{
		parent::__construct();
        $userInfo = $this->session->userdata('userInfo');
        $this->user  = $userInfo;
        $global_vars = array(
            'user' => $this->user
        );
        $this->load->vars($global_vars);
        $this->allow = [];
    }
}
