<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    /** 리스트 array */
    public function getListArray($query,$param = array()){
        $resultObj = $this->db->query($query,$param);
        //        vdp($this->db->last_query());
        if (sizeof($resultObj->result_array()) > 0){
            return $resultObj->result_array();
        }else{
            return false;
        }
    }

    /** 리스트 포함 카운트 array */
    public function getListArrayWithCount($query,$param = array()){

        $listParam = $param;
        array_pop($param);
        array_pop($param);
        $countParam = $param;
        $countResultObj = $this->db->query($query,$countParam);
//        vdp($this->db->last_query());
        $result = array(
            'count' => 0,
            'list' => false,
        );
        if ($countResultObj->num_rows() > 0){
            $result['count'] = $countResultObj->num_rows();
            $query .= " LIMIT ? , ? ";
            $resultObj = $this->db->query($query,$listParam);
//                    vdp($this->db->last_query());
            if (sizeof($resultObj->result_array()) > 0){
                $result['list'] = $resultObj->result_array();
            }else{
                $result['list'] = false;
            }
        }
        return $result;
    }


    /** 로우 Array */
    public function getRowArray($query,$param = array()){
        $resultObj = $this->db->query($query,$param);
//        vdp($this->db->last_query());
        if ($resultObj->num_rows() > 0){
            return $resultObj->row_array();
        }else{
            return false;
        }
    }

    /** 한개의 필드만 가져오기 Array */
    public function getOneField($query,$fieldName ,$param = array()){
        $resultObj = $this->db->query($query,$param);
        //        vdp($this->db->last_query());
        if ($resultObj->num_rows() > 0){
            return $resultObj->row()->$fieldName;
        }else{
            return false;
        }
    }
    
    /** 삽입 */
    public function insertQuery($query,$param){
        $result = $this->db->query($query,$param);
        if($result){
            $lastInsertId = $this->db->insert_id();
            return $lastInsertId;
        }
        return false;
    }
    
    /** 수정 삭제 */
    public function executeQuery($query,$param = array()){
        $executeResult = $this->db->query($query,$param);
            //        vdp($this->db->last_query());
        return $executeResult;
    }

}

/**
 * ADMIN 에서 사용할 Database Load
 * Class ADMIN_Model
 */
class ADMIN_Model extends MY_Model{
    /**
     * 확장성을 대비하여 멀티 DB 지원
     */
     public function __construct()
    {
        parent::__construct();
    }
}

