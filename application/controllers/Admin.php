<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }


    public function Monitor($monitorNum)
    {
        $getMonitorSetup = $this->defaultservice->getMonitorSet($monitorNum);
        $fontList = $this->defaultservice->getFontList();

        $data =array(
            'num' => $monitorNum,
            'info' => $getMonitorSetup['set'],
            'page1Menu' => $getMonitorSetup['menu']['page1'],
            'page2Menu' => $getMonitorSetup['menu']['page2'],
            'page3Menu' => $getMonitorSetup['menu']['page3'],
            'fonts' => $fontList
        );

//        vdp($data);

        $this->load->view('Monitor',$data);
    }

    public function ChangeInfo()
    {
        $getAdmin = $this->defaultservice->getAdmin();
        $data = array(
            'admin' => $getAdmin
        );
        $this->load->view('ChangeInfo',$data);
    }

    public function SaveAdmin(){
        $this->yield = false;
        $this->output->set_content_type('application/json');
        $param = $this->input->post();
        $saveResult= $this->defaultservice->saveAdmin($param);
        echo json_encode($saveResult);
    }

    public function MonitorSave(){
        $this->yield = false;
        $this->output->set_content_type('application/json');
        $param = $this->input->post();
        $uploadResult = $this->defaultservice->multi_file_upload(['bg_img','bg_mov'], 'uploads', $_FILES);
        $imgPath= (isset($uploadResult['bg_img']['file_name']))?"/uploads/".$uploadResult['bg_img']['file_name']:"";
        $movPath= (isset($uploadResult['bg_mov']['file_name']))?"/uploads/".$uploadResult['bg_mov']['file_name']:"";
        $result = $this->defaultservice->monitorSave($param,$imgPath,$movPath);
        echo json_encode($result);
    }
}
