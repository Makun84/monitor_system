<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->yield = false;
        $this->allow = ['index','process'];
    }

    /** 로그인 인트로 */
    public function index()
    {
        $getParam = $this->input->get();
        $url = isset($getParam['url']) ? $getParam['url'] : '';
        $saveId = get_cookie("userSaveId");
        $data = array(
            'LoginId' => '',
            'returnUrl' => $url,
            'saveId' => $saveId
        );
        $this->load->view('Login', $data);
    }

    /** 로그인 처리 */
    public function process(){
        $this->output->set_content_type('application/json');
        $postParam = $this->input->post();
        $processResult = $this->defaultservice->loginProcess($postParam);
        echo json_encode($processResult);
    }


    /** 로그아웃 */
    public function logout(){
        $this->session->unset_userdata('userInfo');
        header('Location: '."/Login");
    }
    

}
