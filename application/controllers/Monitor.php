<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitor extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->layout = 'monitor';
        $this->allow = ['screen'];
    }

    public function screen($monitorNum){
        $getMonitorSetup = $this->defaultservice->getMonitorSet($monitorNum);
        $fontList = $this->defaultservice->getFontList();

        $bg_img = $getMonitorSetup['set']['bg_img'];
        $bg_mov = $getMonitorSetup['set']['bg_mov'];
        $titleFont = "";
        $menuFont = "";
        foreach ($fontList as $font){
            $titleFont = ($getMonitorSetup['set']['titleFont']==$font['idx'])?$font['FontFamily']:$titleFont;
            $menuFont  = ($getMonitorSetup['set']['menuFont']==$font['idx'])?$font['FontFamily']:$menuFont;
        }
        $titleSize = $getMonitorSetup['set']['titleSize'];
        $menuSize = $getMonitorSetup['set']['menuSize'];
        $titleColor = $getMonitorSetup['set']['titleColor'];
        $menuColor = $getMonitorSetup['set']['menuColor'];

        $info = $getMonitorSetup['set'];

        $global_vars = array(
            'titleSize' => $titleSize,
            'menuSize' => $menuSize,
            'titleFont' => $titleFont,
            'menuFont' => $menuFont,
            'titleColor' => $titleColor,
            'menuColor' => $menuColor,
            'bg_type' => $info['templet_type'],
        );
        if($info['templet_type']==1 || $info['templet_type']==3) {
            $global_vars['bg_img'] = $bg_img;
        }else{
            $global_vars['bg_mov'] = $bg_mov;
        }
        $this->load->vars($global_vars);

        //배경 화면 타입 1 이미지 2 동영상 3이미지 배경 동영상 중간 삽입
        $backGroundType = $info['templet_type'];
        $menuBgColor = "";
        if($info['menuBgUse']=="Y"){
            list($r, $g, $b) = sscanf($info['menuBgColor'], "#%02x%02x%02x");
            $menuBgColor =  "background-color:rgba(".$r.",".$g.",".$b.",0.5);";
        }

        //메뉴 사이즈 정의
        $menuWidth = "";
        if($info['text_loc']==1){
            $menuWidth="100";
        }else if($info['text_loc']==2){
            $menuWidth="50";
        }else if($info['text_loc']==3){
            $menuWidth="30";
        }
        
        //메뉴 정렬 정의
        $menuAlign = "";
        if($info['text_type']==1){
            $menuAlign="Left";
        }else if($info['text_type']==2){
            $menuAlign="Center";
        }else if($info['text_type']==3){
            $menuAlign="Right";
        }

        $pageEffect = ($info['pageEffect']==1)?true:false;

        $includeVideo = false;
        if($backGroundType==3 && ($info['text_type'] == 1 || $info['text_type']== 3 ) && isset($info['bg_mov']) && $info['bg_mov']!=''){
            $includeVideo = true;
        }

        $data =array(
            'info' => $info,
            'menu' => $getMonitorSetup['menu'],
            'menuBgColor' => $menuBgColor,
            'menuWidth' => $menuWidth,
            'menuAlign' =>$menuAlign,
            'pageEffect' => $pageEffect,
            'includeVideo' =>$includeVideo
        );

        $this->load->view('defMonitor', $data);

    }

}