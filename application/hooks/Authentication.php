<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication
{
    function checkPermission() {
        $CI =& get_instance();
        $returnUrl = urlencode($CI->uri->ruri_string());

        if (isset($CI->allow) && (is_array($CI->allow) === false OR in_array($CI->router->method, $CI->allow) === false)) {
            if($CI->user==null) {
                header('Location: /' . $CI->loginUrl . "?url=" . $returnUrl);
            }
        }
    }
}
