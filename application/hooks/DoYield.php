<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class DoYield
{
    function initYield()
    {
        global $OUT;
        $CI =& get_instance();
        $output = $CI->output->get_output();
        $CI->yield = isset($CI->yield) ? $CI->yield : TRUE;
        $CI->layout = isset($CI->layout) ? $CI->layout : 'default';
        $CI->layoutLocation = isset($CI->layoutLocation) ? $CI->layoutLocation : '';

        if ($CI->yield === TRUE)
        {

            /** 레이아웃 파일 로드 */
            if (!preg_match('/(.+).php$/', $CI->layout))
            {
                $CI->layout .= '.php';
            }
            $requested = APPPATH . 'views/'.$CI->layoutLocation.'layout/' . $CI->layout;
            $layout = $CI->load->file($requested, true);
            $view = str_replace("{output}", $output, $layout);
        }
        else
        {
            $view = $output;
        }
        $OUT->_display($view);

    }
}
?>