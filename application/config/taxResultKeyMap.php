<?php
/**
 * Created by PhpStorm.
 * User: WebDev04
 * Date: 2018-04-04
 * Time: 오후 3:41
 */
$config['pdfResultMap'] = array(
    'outData1' => array(
        'amt01' => 'A_1',
        'amt02' => 'A_3',
        'amt03' => 'A_5',
        'amt04' => 'A_7',
        'amt05' => 'A_9',
        'amt06' => 'A_2',
        'amt07' => 'A_4',
        'amt08' => 'A_6',
        'amt09' => 'A_8',
        'amt10' => 'A_10'
    ),
    'outData2' => 'B_1',
    'outData3' => 'C_1',
    'outData4' => array(
        'amt1' => 'D_1',
        'amt2' => 'D_2'
    ),
    'outData5' => array(
        'amt1' => 'E_1',
        'amt2' => 'E_6'
    ),
    'outData6' => array(
        '01' => array(
            'amt1' => 'F_A_1',
            'amt2' => 'F_A_2',
            'amt3' => 'F_A_3',
            'amt4' => 'F_A_5',
        ),
        '02' => array(
            'amt1' => 'F_B_1',
            'amt2' => 'F_B_2',
            'amt3' => 'F_B_3',
            'amt4' => 'F_B_5',
        ),
        '03' => array(
            'amt1' => 'F_C_1',
            'amt2' => 'F_C_2',
            'amt3' => 'F_C_3',
            'amt4' => 'F_C_5',
        ),
        '04' => array(
            'amt1' => 'F_D_1',
            'amt2' => 'F_D_2',
            'amt3' => 'F_D_3',
            'amt4' => 'F_D_5',
        ),
        '05' => array(
            'amt1' => 'F_E_1',
            'amt2' => 'F_E_2',
            'amt3' => 'F_E_3',
            'amt4' => 'F_E_5',
        ),
        '06' => array(
            'amt1' => 'F_F_1',
            'amt2' => 'F_F_2',
            'amt3' => 'F_F_3',
            'amt4' => 'F_F_5',
        ),
    ),
    'outData7' => array(
        'subList1' => array(
            '101' => 'G_1',
            '102' => 'G_2',
            '200' => 'G_3',
            '300' => 'G_4',
            '401' => 'G_5',
            '402' => 'G_5',
            '403' => 'G_5',
        ),
        'subList2' => array(
            'name' => 'G_6_1',
            'amt' => 'G_6_2'
        )
    ),
    'outData8' => array(
        'subList1' => array(
            'name' => 'H_1_1',
            'amt' => 'H_1_2'
        ),
        'subList2' => array(
            '202' => 'H_2',
            '236' => 'H_3',
            '201' => 'H_4',
            '208' => 'H_5',
            '203' => 'H_6',
            '273' => 'H_7',
            '289' => 'H_8',
            '290' => 'H_9',
            '274' => 'H_10',
            '275' => 'H_10',
            '276' => 'H_10',
            '277' => 'H_11',
            '291' => 'H_11',
            '278' => 'H_12',
            '279' => 'H_13',
            '282' => 'H_14',
            '283' => 'H_14',
            '284' => 'H_15',
            '210' => 'H_16',
            '243' => 'H_17',
            '246' => 'H_18',
            '281' => 'H_18',
        ),
        'subList3' => array(
            'name' => 'H_19_1',
            'amt' => "H_19_2"
        )
    ),
    'outData9' => array(
        'subList1' => array(
            'I_1' => [1,2],
            'I_2' => [3,4],
            'I_3' => [5],
            'I_4' => [6,7,8,9,10,11,12,13,14,15,16],
            'I_5' => [18,19],
            'I_6' => [20,21],
            'I_7' => [22,23],
            'I_8' => [24,25],
            'I_9' => [26],
            'I_10' => [27,28],
            'I_11' => [29],
            'I_12' => [30,31],
            'I_13' => [32,33],
            'I_14' => [34],
            'I_15' => [35],
            'I_16' => [36],
            'I_17' => [37]
        )
    )

);
$config['taxResultMap'] = array(
    /** 세액 계산  */
    "A_1" => "종합소득 금액",
    "A_2" => "세액공제",
    "A_3" => "소득공제",
    "A_4" => "가산세",
    "A_5" => "과세표준",
    "A_6" => "추가납부세액",
    "A_7" => "산출 세액",
    "A_8" => "종합소득세 납부(환급)할 총 세액",
    "A_9" => "세액감면",
    "A_10" => "지방소득세 납부(환급)할 총 세액",

    /** 이자소득명세서 */
    "B_1" => "이자소득금액 합계	",

    /** 배당소득명세서 */
    "C_1" => "배당소득금액 합계",

    /**  사업소득명세서*/
    "D_1" => "총수입금액 계	",
    "D_2" => "필요경비 계,",

    /** 근로소득·연금소득·기타소득명세서 */
    "E_1" => "총급여액",
    "E_2" => "근로소득공제",
    "E_3" => "총연금액",
    "E_4" => "연금소득공제",
    "E_5" => "총수입금액",
    "E_6" => "필요경비",

    /** 종합소득금액 및 결손금·이월결손금공제명세서 */
    /* 이자소득금액 */
    "F_A_1" => "소득별 소득금액",
    "F_A_2" => "부동산임대업 (주택임대업 제외) 외의 사업소득 결손금 공제금액",
    "F_A_3" => "부동산임대업 (주택 임대업 제외) 외의 사업소득 이월결손금 공제금액",
    "F_A_4" => "부동산 임대업 (주택임대업 제외) 의 사업소득 이월결손금 공제금액",
    "F_A_5" => "결손금 ·이월결손금 공제후 소득금액",

    /* 배당소득금액 */
    "F_B_1" => "소득별 소득금액",
    "F_B_2" => "부동산임대업 (주택임대업 제외) 외의 사업소득 결손금 공제금액",
    "F_B_3" => "부동산임대업 (주택 임대업 제외) 외의 사업소득 이월결손금 공제금액",
    "F_B_4" => "부동산 임대업 (주택임대업 제외) 의 사업소득 이월결손금 공제금액",
    "F_B_5" => "결손금 ·이월결손금 공제후 소득금액",

    /* 사업소득금액 */
    "F_C_1" => "소득별 소득금액",
    "F_C_2" => "부동산임대업 (주택임대업 제외) 외의 사업소득 결손금 공제금액",
    "F_C_3" => "부동산임대업 (주택 임대업 제외) 외의 사업소득 이월결손금 공제금액",
    "F_C_4" => "부동산 임대업 (주택임대업 제외) 의 사업소득 이월결손금 공제금액",
    "F_C_5" => "결손금 ·이월결손금 공제후 소득금액",

    /* 근로소득금액 */
    "F_D_1" => "소득별 소득금액",
    "F_D_2" => "부동산임대업 (주택임대업 제외) 외의 사업소득 결손금 공제금액",
    "F_D_3" => "부동산임대업 (주택 임대업 제외) 외의 사업소득 이월결손금 공제금액",
    "F_D_4" => "부동산 임대업 (주택임대업 제외) 의 사업소득 이월결손금 공제금액",
    "F_D_5" => "결손금 ·이월결손금 공제후 소득금액",

    /* 연금소득금액 */
    "F_E_1" => "소득별 소득금액",
    "F_E_2" => "부동산임대업 (주택임대업 제외) 외의 사업소득 결손금 공제금액",
    "F_E_3" => "부동산임대업 (주택 임대업 제외) 외의 사업소득 이월결손금 공제금액",
    "F_E_4" => "부동산 임대업 (주택임대업 제외) 의 사업소득 이월결손금 공제금액",
    "F_E_5" => "결손금 ·이월결손금 공제후 소득금액",

    /* 기타소득금액 */
    "F_F_1" => "소득별 소득금액",
    "F_F_2" => "부동산임대업 (주택임대업 제외) 외의 사업소득 결손금 공제금액",
    "F_F_3" => "부동산임대업 (주택 임대업 제외) 외의 사업소득 이월결손금 공제금액",
    "F_F_4" => "부동산 임대업 (주택임대업 제외) 의 사업소득 이월결손금 공제금액",
    "F_F_5" => "결손금 ·이월결손금 공제후 소득금액",

    /* 합계 */
    "F_G_1" => "소득별 소득금액",
    "F_G_2" => "부동산임대업 (주택임대업 제외) 외의 사업소득 결손금 공제금액",
    "F_G_3" => "부동산임대업 (주택 임대업 제외) 외의 사업소득 이월결손금 공제금액",
    "F_G_4" => "부동산 임대업 (주택임대업 제외) 의 사업소득 이월결손금 공제금액",
    "F_G_5" => "결손금 ·이월결손금 공제후 소득금액",

    /** 소득공제명세서 */
    "G_1" => "1 + 2 + 3. 인적공제 : 기본공제",
    "G_2" => "4 + 5 + 6 + 7. 인적공제 : 추가공제",
    "G_3" => "9 + 10. 연금보험료공제",
    "G_4" => "11. 주택담보노후연금 이자비용공제",
    "G_5" => "12 + 13 + 14. 특별공제",
    /* 조세특례제한법상 소득공제 (가변) */
    "G_6_1" => "",
    "G_6_2" => "",

    "G_7" => "소득공제 합계",
    "G_8" => "소득공제 종합한도 초과액",

    /** 세액감면명세서 · 세액공제명세서 */
    /* 세액감면 (가변)*/
    "H_1_1" => "",
    "H_1_2" => "",

    "H_2" => "배당세액공제",
    "H_3" => "기장세액공제",
    "H_4" => "외국납부세액공제",
    "H_5" => "재해손실세액공제",
    "H_6" => "근로소득세액공제",
    "H_7" => "자녀세액공제자녀",
    "H_8" => "6세이하자녀세액공제",
    "H_9" => "출산입양자녀세액공제",
    "H_10" => "연금계좌세액공제 (코드 274 + 275 + 276 계)",
    "H_11" => "보험료 (코드 277 + 291 계)",
    "H_12" => "의료비",
    "H_13" => "교육비",
    "H_14" => "기부금 (코드 282 + 283 계)",
    "H_15" => "표준세액공제",
    "H_16" => "납세조합세액공제",
    "H_17" => "전자계산서발급전송세액공제",
    "H_18" => "정치자금기부금 (코드 246 + 281 계)",

    /* 세액공제 (가변)*/
    "H_19_1",
    "H_19_2",

    "I_1" => "무신고",
    "I_2" => "과소신고",
    "I_3" => "납부(환급)불성실",
    "I_4" => "보고불성실",
    "I_5" => "증빙불비",
    "I_6" => "영수증수취명세서 미제출",
    "I_7" => "사업장현황신고 불성실",
    "I_8" => "공동사업장등록 불성실",
    "I_9" => "무기장",
    "I_10" => "사업용계좌 미신고 등",
    "I_11" => "신용카드 거부",
    "I_12" => "현금영수증 미발급",
    "I_13" => "기부금 영수증 불성실",
    "I_14" => "동업기업배분가산세",
    "I_15" => "성실신고확인서미제출가산세",
    "I_16" => "유보소득계산명세서제출불성실가산세",
    "I_17" => "가산세 합계",
);
