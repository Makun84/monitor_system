<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
/** 레이아웃 설정파일 */
$hook['display_override'][] = array(
    'class'    => 'DoYield',
    'function' => 'initYield',
    'filename' => 'DoYield.php',
    'filepath' => 'hooks'
);

/** 권한 처리 */
$hook['post_controller_constructor'][] = array(
    'class'     => 'Authentication',
    'function'  => 'checkPermission',
    'filename'  => 'Authentication.php',
    'filepath'  => 'hooks'
);

