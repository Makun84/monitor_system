<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['per_page'] = 10;
$config['enable_query_strings'] = true;
$config['num_links'] = 5;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
$config['query_string_segment'] = 'page';
$config['full_tag_open'] = "<ul class=\"pagination\">";
$config['full_tag_close'] = "</ul>";
$config['cur_tag_open'] = "<li class=\"paginate_button active\"><a href='javascript:void(0)'>";
$config['cur_tag_close'] = "</a></li>";
$config['num_tag_open'] = "<li class=\"paginate_button\">";
$config['num_tag_close'] = "</li>";

$config['first_tag_open'] = "<li class=\"paginate_button previous\">";
$config['first_link'] = "<i class=\"fa fa-angle-double-left\"></i>";
$config['first_tag_close'] = "</li>";

$config['last_tag_open'] = "<li class=\"paginate_button next\">";
$config['last_link'] = "<i class=\"fa fa-angle-double-right\"></i>";
$config['last_tag_close'] = "</li>";
$config['reuse_query_string'] = true;

$config['prev_tag_open'] = "<li class=\"paginate_button previous\">";
$config['prev_link'] = "<i class=\"fa fa-angle-left\"></i>";
$config['first_tag_close'] = "</li>";
$config['next_tag_open'] = "<li class=\"paginate_button next\">";
$config['next_link'] = "<i class=\"fa fa-angle-right\"></i>";
$config['next_tag_close'] = "</li>";
/* End of file pagination.php */
/* Location: ./application/config/pagination.php */