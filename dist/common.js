$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

Number.prototype.format = function(){
    if(this==0) return 0;

    var reg = /(^[+-]?\d+)(\d{3})/;
    var n = (this + '');

    while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

    return n;
};

String.prototype.format = function(){
    var num = parseFloat(this);
    if( isNaN(num) ) return "0";

    return num.format();
};

function getJson(url,dataObj,sync) {
    var result = {};
    $.ajax({
        url: url,
        type: 'post',
        data: dataObj,
        async: sync,
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            result = data;
        }
    });
    return result;
}

function getJsonFile(url,dataObj,sync) {
    var result = {};
    $.ajax({
        url: url,
        type: "post",
        data: dataObj,
        processData : false,
        contentType : false,
        async: sync,
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            result = data;
        }
    });
    return result;
}

$(document).ready(function(){
    var fileTarget = $('.filebox .upload-hidden');
    fileTarget.on('change', function(){ // 값이 변경되면
     if(window.FileReader){ // modern browser
        var filename = $(this)[0].files[0].name;
     } else {// old IE
        var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출 } // 추출한 파일명 삽입 $(this).siblings('.upload-name').val(filename); }); });
     }
    // 추출한 파일명 삽입
     $(this).siblings('.upload-name').val(filename);
    });
});

$(document).ready(function () {
    $("input[type=number]").on("input",function () {
        if(typeof $(this).attr("maxlength") !==  'undefined' ) {
            var maxLength = $(this).attr("maxlength");
            if ($(this).val().length > maxLength) {
                $(this).val($(this).val().slice(0,maxLength));
            }
        }
    });
});

function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}

var allowFileExteinsions =['docx','doc','xlsx','xls','pptx','ppt','pdf','hwp','jpg','bmp','gif','png','zip','txt','rtf'];

function getFileExtIcon(ext) {
    /** @TODO 아이콘 다 넘어오면 그에 맞는 아이콘 지정..**/
    var iconImg = "";
    switch (ext){
        case "doc":
            iconImg = "/dist/img/icon/docIcon.png";
            break;
        case "docx":
            iconImg = "/dist/img/icon/docIcon.png";
            break;
        case "txt":
            iconImg = "/dist/img/icon/txtIcon.png";
            break;
        case "rtf":
            iconImg = "/dist/img/icon/etcIcon.png";
            break;
        case "pdf":
            iconImg = "/dist/img/icon/pdfIcon.png";
            break;
        case "xls":
            iconImg = "/dist/img/icon/excelIcon.png";
            break;
        case "xlsx":
            iconImg = "/dist/img/icon/excelIcon.png";
            break;
        case "csv":
            iconImg = "/dist/img/icon/excelIcon.png";
            break;
        case "png" :
            iconImg = "/dist/img/icon/imgIcon.png";
            break;
        case "jpg" :
            iconImg = "/dist/img/icon/imgIcon.png";
            break;
        case "jpeg" :
            iconImg = "/dist/img/icon/imgIcon.png";
            break;
        case "bmp" :
            iconImg = "/dist/img/icon/imgIcon.png";
            break;
        case "gif" :
            iconImg = "/dist/img/icon/imgIcon.png";
            break;
        case "hwp" :
            iconImg = "/dist/img/icon/hwpicon.png";
            break;
        case "ppt" :
            iconImg = "/dist/img/icon/pptIcon.png";
            break;
        case "pptx" :
            iconImg = "/dist/img/icon/pptIcon.png";
            break;
        default :
            iconImg = "/dist/img/icon/etcIcon.png";
            break;
    }
    return iconImg;
}

function nextChar(c) {
    var i = (parseInt(c, 36) + 1 ) % 36;
    return (!i * 10 + i).toString(36);
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(cookieName) {
    var ck = getCookie(cookieName);
    if (ck.length > 5) {
        return true;
    }
    return false;
}