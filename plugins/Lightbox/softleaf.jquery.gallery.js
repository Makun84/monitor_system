
(function ($) {
    $.softLeafGallery = {
        selectTargetArea : '',
        /** 사용자 변경 가능값 **/
        targetClass : ".thumb_gallery_btn",
        imgUrlAttr: "src",
        time: 400,
        fieldArea : 'softleaf_gallery_over_area',

        nextClass : '',
        prevClass : '',
        closeClass : '',

        /** 처리를 위한 값 **/
        arrayImages : [],

        /** 화면 처리를 위한 오브젝트 생성 **/
        bgOverLayObj : {},
        wrapperHtmlObj : {},
        viewerBodyHtmlObj : {},
        currentImgObj : {},
        closeBtnObj : {},
        prevBtnObj : {},
        nextBtnObj : {},
        uniqueId : '',

        activeImageArr : [],

        setval : function(fieldId,override){
            var randomUniqueId ="softleaf_gallery_"+Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
            this.uniqueId = randomUniqueId;

            /** 사용자가 지정한 값이 있다면 지정한 값으로 변경 **/
            this.targetClass = (typeof override.targetClass !=="undefined") ? "."+override.targetClass : this.targetClass;
            this.time = (typeof override.time !=="undefined") ? override.time : this.time;
            this.fieldArea = (typeof override.fieldArea !=="undefined") ? override.fieldArea : this.fieldArea;

            /** 버튼 정의 **/
            this.closeClass = (typeof override.closeClass !=="undefined") ? override.closeClass : "closebtn";
            var closeTxt = (typeof override.closeTxt !=="undefined") ? override.closeTxt : "&#x2715;";
            this.prevClass = (typeof override.prevClass !=="undefined") ? override.prevClass : "previousImage";
            var prevTxt = (typeof override.prevTxt !=="undefined") ? override.prevTxt : "&#x276C;";
            this.nextClass = (typeof override.nextClass !=="undefined") ? override.nextClass :  "nextImage";
            var nextTxt = (typeof override.nextTxt !=="undefined") ? override.nextTxt : "&#x276D;";

            /** 기본 레이아웃 스타일 적용 **/
            /** 이미지 레이어 팝업 html 생성 **/
            this.bgOverLayObj = $("<div>");
            this.bgOverLayObj.attr("id",randomUniqueId);
            this.bgOverLayObj.css("display","none");
            this.bgOverLayObj.css("position","fixed");
            this.bgOverLayObj.css("top",0);
            this.bgOverLayObj.css("right",0);
            this.bgOverLayObj.css("bottom",0);
            this.bgOverLayObj.css("left",0);
            this.bgOverLayObj.css("width","100%");
            this.bgOverLayObj.css("height","100%");
            this.bgOverLayObj.css("background","rgba(0,0,0, 0.9)");
            this.bgOverLayObj.css("z-index",1050);
            this.bgOverLayObj.css("overflow","auto");

            this.wrapperHtmlObj = $("<div>");
            this.wrapperHtmlObj.addClass("container_img");

            this.viewerBodyHtmlObj = $("<div>");
            this.viewerBodyHtmlObj.addClass("con-ac-ar");

            /** 현재 보고 있는 이미지 **/
            this.currentImgObj = $("<img>");
            this.currentImgObj.addClass("activePopUpImg");

            /** 닫힘 버튼 **/
            this.closeBtnObj = $("<div>");
            this.closeBtnObj.addClass(this.closeClass);
            this.closeBtnObj.attr("parentAreaId",this.uniqueId);
            this.closeBtnObj.attr("id",this.uniqueId+"_closeBtn");
            this.closeBtnObj.append(closeTxt);

            /** 이전 버튼 */
            this.prevBtnObj = $("<div>");
            this.prevBtnObj.addClass(this.prevClass);
            this.prevBtnObj.attr("parentAreaId",this.uniqueId);
            this.prevBtnObj.attr("id",this.uniqueId+"_prevBtn");
            this.prevBtnObj.append(prevTxt);

            /** 다음 버튼 */
            this.nextBtnObj = $("<div>");
            this.nextBtnObj.addClass(this.nextClass);
            this.nextBtnObj.attr("parentAreaId",this.uniqueId);
            this.nextBtnObj.attr("id",this.uniqueId+"_nextBtn");
            this.nextBtnObj.append(nextTxt);

            /** 이미지 로드 작업 **/
            var __targetClassName = this.targetClass;
            var imgLoc = this.imgUrlAttr;
            var tempArrImages = [];
            $(fieldId).find(__targetClassName).each(function () {
                var attrSrc = $(this).attr(imgLoc);
                $(this).attr("parentAreaId",randomUniqueId);
                tempArrImages.push(attrSrc);
            });

            this.arrayImages[randomUniqueId] = tempArrImages;

        },
        showImage : function(activeFiledId) {
            var _this = this;
            var setTime = _this.time;
            var ActiveLayer = $("#"+activeFiledId);
            ActiveLayer.fadeIn(setTime);
        },
        nextBtn : function (ActiveImgObj) {
            var _this = this;
            var firstItem = _this.activeImageArr[0];// taking first item in array
            var lastitem =  _this.activeImageArr[_this.activeImageArr.length - 1];// taking last item in array

            for (var i = 0; i < _this.activeImageArr.length; i++) {
                if (ActiveImgObj.attr("src") == _this.activeImageArr[i] && ActiveImgObj.attr("src") !==  lastitem) {
                    ActiveImgObj.attr("src", _this.activeImageArr[i + 1]);
                    break;
                }
                if(ActiveImgObj.attr("src") == lastitem){
                    ActiveImgObj.attr("src",firstItem);
                    break;
                }
            }
        },
        prevBtn : function (ActiveImgObj) {
            var _this = this;
            var firstItem = _this.activeImageArr[0];// taking first item in array
            var lastitem =  _this.activeImageArr[_this.activeImageArr.length - 1];// taking last item in array
            for (var i = 0; i <  _this.activeImageArr.length; i++) {
               if(ActiveImgObj.attr("src") == firstItem){
                   ActiveImgObj.attr("src",lastitem);
                    break;
                }
                if (ActiveImgObj.attr("src") ==  _this.activeImageArr[i] && ActiveImgObj.attr("src") !== firstItem) {
                    ActiveImgObj.attr("src",  _this.activeImageArr[i - 1]);
                    break;
                }
            }
        },
        init: function (fieldId , override ) {
            override = (typeof override ==="undefined" )?{}:override;
            this.setval(fieldId ,override);
            // open image
            var _this = this;

            $(fieldId).find(_this.targetClass).on("click",function () {
                var attr_src = $(this).attr(_this.imgUrlAttr);
                var nowFieldId = $(this).attr("parentAreaId");
                _this.activeImageArr = _this.arrayImages[nowFieldId];
                var ActiveImgObj = $("#"+nowFieldId).find(".activePopUpImg");
                var ActiveNextBtn = $("#"+nowFieldId).find("#"+nowFieldId+"_nextBtn");
                var ActivePrevBtn = $("#"+nowFieldId).find("#"+nowFieldId+"_prevBtn");
                var ActiveCloseBtn = $("#"+nowFieldId).find("#"+nowFieldId+"_closeBtn");
                var ActiveLayer = $("#"+nowFieldId);

                for (var i = 0; i <  _this.activeImageArr.length; i++) {
                    if ( _this.activeImageArr[i] == attr_src) {
                        ActiveImgObj.attr("src",attr_src);
                        _this.showImage(nowFieldId);
                        break;
                    }
                }

                //next Image
                ActiveNextBtn.on("click",function () {
                    _this.nextBtn(ActiveImgObj);
                });

                //previous Image
                ActivePrevBtn.on("click",function () {
                    _this.prevBtn(ActiveImgObj);
                });

                //close image
                ActiveCloseBtn.on("click",function () {
                    ActiveLayer.fadeOut(_this.time);
                });

            });

            /** 레이어 셋팅 **/
            _this.viewerBodyHtmlObj.append(_this.prevBtnObj);
            _this.viewerBodyHtmlObj.append(_this.currentImgObj);
            _this.viewerBodyHtmlObj.append(_this.nextBtnObj);
            _this.wrapperHtmlObj.append(_this.closeBtnObj);
            _this.wrapperHtmlObj.append(_this.viewerBodyHtmlObj);
            _this.bgOverLayObj.append(_this.wrapperHtmlObj);

            var fieldName = _this.fieldArea;
            var fieldObj = $("#"+fieldName);
            fieldObj.append(_this.bgOverLayObj);

        },
    };
})(jQuery);